import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({super.key});

  int daysBetween(DateTime from, DateTime to) {
    from = DateTime(from.year, from.month, from.day);
    to = DateTime(to.year, to.month, to.day);
    return (to.difference(from).inHours / 24).round();
  }

  //the birthday's date
  final birthday = DateTime(2023, 03, 05);

  final List<String> book = [
    'The Flameseeker Prophecies',
    'Shiro\'s Return',
    'Night Falls',
    'Hero\'s Handbook',
    'The young Heroes of Tyria'
  ];

  final List<String> maxZaishenCoins = [
    '70',
    '74',
    '100',
    '105',
    '150'
  ];

  final List<String> missionListEN = [
    'Fort Ranik',
    'A Gate Too Far',
    'Minister Cho\'s Estate',
    'Thunderhead Keep',
    'Tihark Orchard',
    'Finding the Bloodstone',
    'Dunes of Despair',
    'Vizunah Square',
    'Jokanur Diggings',
    'Iron Mines of Moladune',
    'Kodonur Crossroads',
    'G.O.L.E.M.',
    'Arborstone',
    'Gates of Kryta',
    'Gate of Madness',
    'The Elusive Golemancer',
    'Riverside Province',
    'Boreas Seabed',
    'Ruins of Morah',
    'Hell\'s Precipice',
    'Ruins of Surmia',
    'Curse of the Nornbear',
    'Sunjiang District',
    'Elona Reach',
    'Gate of Pain',
    'Blood Washes Blood',
    'Bloodstone Fen',
    'Jennur\'s Horde',
    'Gyala Hatchery',
    'Abaddon\'s Gate',
    'The Frost Gate',
    'Augury Rock',
    'Grand Court of Sebelkeh',
    'Ice Caves of Sorrow',
    'Raisu Palace',
    'Gate of Desolation',
    'Thirsty River',
    'Blacktide Den',
    'Against the Charr',
    'Abaddon\'s Mouth',
    'Nundu Bay',
    'Divinity Coast',
    'Zen Daijun',
    'Pogahn Passage',
    'Tahnnakai Temple',
    'The Great Northern Wall',
    'Dasha Vestibule',
    'The Wilds',
    'Unwaking Waters',
    'Chahbek Village',
    'Aurora Glade',
    'A Time for Heroes',
    'Consulate Docks',
    'Ring of Fire',
    'Nahpui Quarter',
    'The Dragon\'s Lair',
    'Dzagonur Bastion',
    'D\'Alessio Seaboard',
    'Assault on the Stronghold',
    'The Eternal Grove',
    'Sanctum Cay',
    'Rilohn Refuge',
    'Warband of Brothers',
    'Borlis Pass',
    'Imperial Sanctum',
    'Moddok Crevice',
    'Nolani Academy',
    'Destruction\'s Depths',
    'Venta Cemetery'
  ];

  final List<String> shiningBladeListEN = [
    'Maximilian the Meticulous',
    'Joh the Hostile',
    'Barthimus the Provident',
    'Calamitous',
    'Greves the Overbearing',
    'Lev the Condemned',
    'Justiciar Marron',
    'Justiciar Kasandra',
    'Vess the Disputant',
    'Justiciar Kimii',
    'Zaln the Jaded',
    'Justiciar Sevaan',
    'Insatiable Vakar',
    'Amalek the Unmerciful',
    'Carnak the Hungry',
    'Valis the Rampant',
    'Cerris',
    'Sarnia the Red-Handed',
    'Destor the Truth Seeker',
    'Selenas the Blunt',
     'Justiciar Amilyn'
  ];

  final List<String> column4 = [
    'Verata',
    'Droajam, Mage of the Sands',
    'Royen Beastkeeper',
    'Eldritch Ettin',
    'Vengeful Aatxe',
    'Fronis Irontoe',
    'Urgoz',
    'Fenrir',
    'Selvetarm',
    'Mohby Windbeak',
    'Charged Blackness',
    'Rotscale',
    'Zoldark the Unholy',
    'Korshek the Immolated',
    'Myish, Lady of the Lake',
    'Frostmaw the Kinslayer',
    'Kunvie Firewing',
    'Z\'him Monns',
    'The Greater Darkness',
    'TPS Regulator Golem',
    'Plague of Destruction',
    'The Darknesses',
    'Admiral Kantoh',
    'Borrguus Blisterbark',
    'Forgewight',
    'Baubao Wavewrath',
    'Joffs the Mitigator',
    'Chung, the Attuned',
    'Lord Jadoth',
    'Nulfastu, Earthbound',
    'The Iron Forgeman',
    'Mobrin, Lord of the Marsh',
    'Jarimiya the Unmerciful',
    'Duncan the Black',
    'Quansong Spiritspeak',
    'The Stygian Underlords',
    'Fozzy Yeoryios',
    'The Black Beast of Arrgh',
    'Arachni',
    'The Four Horsemen',
    'Remnant of Antiquities',
    'Arbor Earthcall',
    'Prismatic Ooze',
    'Lord Khobay',
    'Jedeh the Mighty',
    'Ssuns, Blessed of Dwayna',
    'Justiciar Thommis',
    'Harn and Maxine Coldstone',
    'Pywatt the Swift',
    'Fendi Nin',
    'Mungri Magicbox',
    'Priest of Menzies',
    'Kephket Marrowfeast',
    'Commander Wahli',
    'Kanaxai',
    'Khabuus',
    'Molotov Rocktail',
    'The Stygian Lords',
    'Dragon Lich',
    'Havok Soulwail',
    'Ghial the Bone Dancer',
    'Murakai, Lady of the Night',
    'Rand Stormweaver'
  ];

  final List<String> column5 = [
    'Ascalonian Noble',
    'Undead',
    'Blazefiend Griefblade',
    'Farmer Hamnet',
    'Charr',
    'Countess Nadya',
    'Footman Tate',
    'Bandits',
    'Utini Wupwup'
  ];

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        body: SingleChildScrollView(
          scrollDirection: Axis.horizontal,
          child: DataTable(
            columns: const [
              DataColumn(label: Text('Date')),
              DataColumn(label: Text('Mission')),
              DataColumn(label: Text('ZCoins')),
              DataColumn(label: Text('Book')),
              DataColumn(label: Text('Bounty')),
            ],
            rows: List.generate(20, (index) {
              int dateWindow = 5;
              int distanceInDays = daysBetween(birthday, DateTime.now().add(Duration(days: index - dateWindow)));

              return DataRow(
                cells: [
                  DataCell(Text(DateUtils.dateOnly(DateTime.now().add(Duration(days: index - dateWindow))).toString().substring(0, 10))),
                  DataCell(Text(missionListEN[distanceInDays % missionListEN.length].toString())),
                  DataCell(Text(shiningBladeListEN[distanceInDays % shiningBladeListEN.length].toString())),
                  DataCell(Text(column4[distanceInDays % column4.length])),
                  DataCell(Text(column5[distanceInDays % column5.length])),
                ],
              );
            }),
          ),
        ),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});

  // This widget is the home page of your application. It is stateful, meaning
  // that it has a State object (defined below) that contains fields that affect
  // how it looks.

  // This class is the configuration for the state. It holds the values (in this
  // case the title) provided by the parent (in this case the App widget) and
  // used by the build method of the State. Fields in a Widget subclass are
  // always marked "final".

  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _counter = 0;

  void _incrementCounter() {
    setState(() {
      // This call to setState tells the Flutter framework that something has
      // changed in this State, which causes it to rerun the build method below
      // so that the display can reflect the updated values. If we changed
      // _counter without calling setState(), then the build method would not be
      // called again, and so nothing would appear to happen.
      _counter++;
    });
  }

  @override
  Widget build(BuildContext context) {
    // This method is rerun every time setState is called, for instance as done
    // by the _incrementCounter method above.
    //
    // The Flutter framework has been optimized to make rerunning build methods
    // fast, so that you can just rebuild anything that needs updating rather
    // than having to individually change instances of widgets.
    return Scaffold(
      appBar: AppBar(
        // Here we take the value from the MyHomePage object that was created by
        // the App.build method, and use it to set our appbar title.
        title: Text(widget.title),
      ),
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          // Column is also a layout widget. It takes a list of children and
          // arranges them vertically. By default, it sizes itself to fit its
          // children horizontally, and tries to be as tall as its parent.
          //
          // Invoke "debug painting" (press "p" in the console, choose the
          // "Toggle Debug Paint" action from the Flutter Inspector in Android
          // Studio, or the "Toggle Debug Paint" command in Visual Studio Code)
          // to see the wireframe for each widget.
          //
          // Column has various properties to control how it sizes itself and
          // how it positions its children. Here we use mainAxisAlignment to
          // center the children vertically; the main axis here is the vertical
          // axis because Columns are vertical (the cross axis would be
          // horizontal).
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            const Text(
              'You have pushed the button this many times:',
            ),
            Text(
              '$_counter',
              style: Theme.of(context).textTheme.headlineMedium,
            ),
          ],
        ),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: _incrementCounter,
        tooltip: 'Increment',
        child: const Icon(Icons.add),
      ), // This trailing comma makes auto-formatting nicer for build methods.
    );
  }
}
